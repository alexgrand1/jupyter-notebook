
# Jupyter Notebook Explorer

A simple list of Jupyter Notebooks used to explore different features of python
Data Science.




## Installation


Software prerequisites:
- [x]  python ^3.10 installed
- [x]  [poetry](https://python-poetry.org/) installed

Install environment:
```bash
poetry install
```

Run Jupyter Notebook:
```bash
poetry run jupyter notebook
```


    